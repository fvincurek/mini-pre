    <!-- jQuery, loaded in the recommended protocol-less way -->
    <!-- more http://www.paulirish.com/2010/the-protocol-relative-url/
    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
    -->

    <!-- define the project's URL (to make AJAX calls possible, even when using this in sub-folders etc) -->
    <script>
        var url = "<?php echo URL; ?>";
    </script>

    <!-- our JavaScript -->
    <script src="<?php echo URL; ?>jquery-ui/jquery-ui.min.js"></script>
    <script src="<?php echo URL; ?>hallo/rangy/rangy-core.js"></script>
    <script src="<?php echo URL; ?>hallo/jquery.htmlClean.js"></script>

    <script src="<?php echo URL; ?>js/underscore.js"></script>
    <script src="<?php echo URL; ?>loadingbar/jquery.loadingbar.js"></script>
    <script src="<?php echo URL; ?>js/foundation.min.js"></script>
    <script src="<?php echo URL; ?>hallo/hallo.js"></script>
    <script>
      $(document).foundation();
    </script>
    <!-- app scripts -->
    <script src="<?php echo URL; ?>js/application.js"></script>
  </body>
</html>
