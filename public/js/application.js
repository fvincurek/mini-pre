
$(function() {
    console.log('DUH!');
    var API = API || {};
    var edited_content = [];
    var edit_content_holder = [];
    var orig_data;

    API.contents = {
      getAll: function(){
        $.get( GLOBAL_URL+"content/", function( data ) {
            //console.log(data);
            orig_data = data;
            //$('*[pre-id]').fadeOut('fast');
            for (var i = 0; i < data.length; i++) {
                if(data[i].value === ''){
                    //$('#'+data[i].data_id).html('please edit this value');
                    $('*[pre-id="'+data[i].data_id+'"]').html('Edit me!');
                }else{
                    //$('#'+data[i].data_id).html(data[i].value);
                    $('*[pre-id="'+data[i].data_id+'"]').html(data[i].value);
                    //$('*[pre-id="'+data[i].data_id+'"]').text(data[i].value);
                    //console.log('*[pre-id="'+data[i].data_id+'"]: '+data[i].value);
                }
            }
        }, "json" );
      },
      updateContents: function(){
        /*
        console.log('edited_content:');
        console.log(edited_content);
        */
        if (!_.isEmpty(edited_content)){
            $.post( GLOBAL_URL+"content/update", { update_data: edited_content }, function( data ) {
                edited_content = [];
                $('#save-button').fadeOut('fast');
                $('#cancel-button').fadeOut('fast');
                /*
                console.log( data );
                console.log( edited_content );
                */
            }, "json");
        }
      },
      cancelEdit: function(){
        /*
        console.log('edited_content:');
        console.log(edited_content);
        */
        if (!_.isEmpty(orig_data)){
            for (var i = 0; i < orig_data.length; i++) {
                if(orig_data[i].value === ''){
                    //$('#'+orig_data[i].data_id).html('please edit this value');
                    $('*[pre-id="'+orig_data[i].data_id+'"]').html('Edit me!');
                }else{
                    //$('#'+orig_data[i].data_id).html(orig_data[i].value);
                    $('*[pre-id="'+orig_data[i].data_id+'"]').html(orig_data[i].value);
                    //$('*[pre-id="'+data[i].data_id+'"]').text(data[i].value);
                    //console.log('*[pre-id="'+data[i].data_id+'"]: '+data[i].value);
                }
                edited_content = [];
                $('#save-button').fadeOut('fast');
                $('#cancel-button').fadeOut('fast');
            }
        }
      }
    };
    API.contents.getAll();

    $( ".show-admin" ).click(function() {
        $( ".admin" ).slideToggle( "fast", function() {
        // Animation complete.
        });
        $( this ).toggleClass( "light" );
    });


    function loadPage(page) {
        $( "#data-content" ).load( page, function( response, status, xhr ) {

                console.log( status );
                //console.log( response );

            if ( status == "error" ) {
                var msg = "Sorry but there was an error: ";
                $( "#error" ).html( msg + xhr.status + " " + xhr.statusText );
            }
        });
    };

    $('a').click(function(){
        var link = $(this).attr('href');
        loadPage(link);
        return false
    });

    $( ".editable" ).each(function( index ) {
        //console.log( index + ": " + $( this ).text() );
        //edited_content[index] = {data_id: $( this ).attr('id'), value: $( this ).html()};
        if ($( this ).html() === '' || $( this ).html() === '<br>'){ $( this ).html('Please edit this default dummy text (you need to login).'); }
        $( this ).addClass('mousetrap');
    });

    function start_editor() {
        $('.editable').hallo({
          plugins: {
            'halloindicator': {},
            'halloformat': {},
            'halloheadings': {},
            'hallojustify': {},
            'hallolists': {},
            'hallolink': {},
            'halloreundo': {}/*,
            'halloimage': {
                search: function(query, limit, offset, successCallback) {
                    response = {offset: offset, total: limit + 1, assets: searchresult.slice(offset, offset+limit)};
                    successCallback(response);
                },
                suggestions: null,
                uploadUrl: function() {
                  return '/some/example/url'
                }
            }*/
            ,
            'hallocleanhtml': {
              format: true,
              allowedTags: ['pre', 'p', 'em', 'strong', 'br', 'div', 'ol', 'ul', 'li', 'a'],
              allowedAttributes: ['style']
            }
          },
          editable: true,
          toolbar: 'halloToolbarFixed'
        }).hallo('protectFocusFrom', $('#enable'));

        $('.editable').bind('hallomodified', function(event, data) {
            //console.log(data.content);
            var this_id = $(this).attr('pre-id');
            var id_data = _.findWhere(edited_content, {data_id: this_id});
            if(!id_data){
                var push_elem = {data_id: this_id, value: data.content};
                edited_content.push(push_elem);
                //console.log(edited_content);
            }else{
                var index = _.indexOf(edited_content, id_data);            
                _.extend(edited_content[index], {value: data.content});
            }
            if(_.isEmpty(edited_content)){
                $('#save-button').fadeOut('fast');
                $('#cancel-button').fadeOut('fast');
            }else{
                var admin_visible = $('.admin').is(':visible');
                if (!admin_visible){
                    $( ".show-admin" ).click();
                }
                $('#save-button').fadeIn('fast');
                $('#cancel-button').fadeIn('fast');
            }
        });

        $('.editable').bind('hallorestored', function(event, data) {
            $('#modified').html("restored");
            edited_content = [];
        });

        $('.editable').bind('halloselected', function(event, data) {
            $('#modified').html("Selection made");
        });
        $('.editable').bind('hallounselected', function(event, data) {
            $('#modified').html("Selection removed");
        });
    }

    $('#enable').button().click(start_editor);

    $('#enable').button().click();

    $('#save-button').button().click(API.contents.updateContents);

    $('#cancel-button').button().click(API.contents.cancelEdit);

    $('#load-page').button().click(loadPage);

    $('#disable').button().click(function() {
        $('.editable').hallo({editable: false});
    });

    $('.editable').bind('hellodeactivated', function() {
        $(this).hallo({editable:false});
    });

    // map multiple combinations to the same callback
    Mousetrap.bind(['ctrl+alt+s','command+alt+s'], function() {
        //console.log('ALT + S: Save editted content');
        API.contents.updateContents();
        // return false to prevent default browser behavior
        // and stop event from bubbling
        return false;
    });
    
    $(".ajax-call").loadingbar({
      replaceURL: false, /* You can visibly change the URL of the browser to reflect the clicked links by toggling this to true. Default is false. May not work in old browsers. */
        target: "document", /* The container's selector where you want the ajax result to appear. Default is #loadingbar-frame */
        direction: "right", /* The direction where the the loading bar will progress. Default is right. */

        /* Default Ajax Parameters.  */
        async: true, 
        complete: function(xhr, text) {},
        cache: true,
        error: function(xhr, text, e) {},
        global: true,
        headers: {},
        statusCode: {},
        success: function(data, text, xhr) {},
        dataType: "html",
        done: function(data) {}
    });

});
